<?php include("db.php") ?>

<?php include("includes/header.php") ?>

<div class="container p-4">
    <div class="row">
        <div class="col-md-4">

            <?php if(isset($_SESSION['message'])) {?>

                <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">
                    <?= $_SESSION['message'] ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>

                <?php session_unset(); } ?>

            <div class="card card-body">
                <form action="save_users.php" method="POST">
                    <input type="text" class="d-none" name="idUsuarios" id="inputId" value="">
                    <div class="form-group">
                        <input type="text" id="inputNombre" name="nombre" class="form-control" placeholder="Nombre" autofocus>
                    </div>
                    <div class="form-group mt-2">
                        <input type="text" id="inputEmail" name="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group mt-2">
                        <input type="text" id="inputPassword" name="password" class="form-control" placeholder="contraseña">
                    </div>
               
                    <input type="submit" id="btnGuardar" class="mt-2 btn btn-success btn-block" name="save_task" value="Guardar">
                    <input type="submit" id="btnEditar" class="btn btn-warning mt-2 mx-4 d-none"  name="save_task" value="Editar">
                </form>
            </div>
        </div>
        <div class="col-md-8">
        <table class="table">
  <thead>
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Contraseña</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <?php 
    $query = "SELECT *FROM usuarios";
    $result = mysqli_query($conn, $query);

    while($row = mysqli_fetch_array($result)){ ?>
    <tr>
        <th scope="row"><?php echo $row['idUsuarios']?></th>
      <td><?php echo $row['nombre']?></td>
      <td><?php echo $row['email']?></td>
      <td><?php echo $row['password']?></td>
      <td>
        <a href="delete_users.php?id=<?php echo $row['idUsuarios'] ?>" class="btn btn-danger">Eliminar</a>
      </td>
      </tr>
        <?php } ?>
    
      
   
  </tbody>
</table>
        </div>
    </div>
</div>

<?php include("includes/footer.php") ?>