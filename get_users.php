<?php
include("db.php");

header('Content-Type: application/json'); // Asegúrate de que la respuesta sea JSON

if(isset($_GET['name'])){
    $name = intval($_GET['name']); // Asegúrate de que el id sea un número entero
    $query = "SELECT * FROM usuarios WHERE nombre = $name"; // Corrección en la consulta
    $result = mysqli_query($conn, $query);
    
    if(!$result){
        echo json_encode(["error" => "Query Failed"]);
        exit;
    }
    
    $user = mysqli_fetch_assoc($result);
    echo json_encode($user);
} else {
    echo json_encode(["error" => "No id provided"]);
}
?>
