document.addEventListener("DOMContentLoaded", function() {

    const inputNombre = document.getElementById("inputNombre");

    inputNombre.addEventListener("blur", () => {if(inputNombre.value)consultar(inputNombre.value)});
    
})




function consultar(name) {
    fetch(`get_users.php?name=${name}`)
    .then(response => response.json())
    .then(data => {
        if (data.error) {
            console.error(data.error);
        } else {
            console.log(data);
            document.getElementById('inputId').value = data.idUsuarios;
            document.getElementById('inputEmail').value = data.email;
            document.getElementById('inputPassword').value = data.password;
            document.getElementById('btnEditar').classList.remove('d-none');
            document.getElementById('btnGuardar').classList.add('d-none');
            // Aquí puedes actualizar tu interfaz de usuario con los datos del usuario
         //   document.getElementById("userData").innerText = JSON.stringify(data, null, 2);
        }
    })
    .catch(error => console.error('Error:', error));
}


function editar() {
  
            let userId = document.getElementById("inputId").value;
            let nombre = document.getElementById("inputNombre").value;
            let email = document.getElementById("inputEmail").value;
            let password = document.getElementById("inputPassword").value;
    
            let data = {
                idUsuarios: userId,
                nombre: nombre,
                email: email,
                password: password
            };
            console.log(data);
            fetch('edit_users.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                if (data.type === "success") {
                    alert(data.message);
                } else {
                    alert("Error: " + data.message);
                }
            })
           
 
    
}