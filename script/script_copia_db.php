<?php
// Configuración de la base de datos
$servername = "localhost";
$username = "root";
$password = "anlug1234";
$database = "php_mysql";

// Ruta completa al comando mysqldump
$mysqldumpPath = "C:\Program Files (x86)\MySQL\MySQL Server 5.7\bin\mysqldump.exe";

// Ruta de la carpeta donde se almacenarán las copias de seguridad
$backupFolder = "C:/Users/USUARIO/Documents/dumps";
$backupFileName = "backup_" . date("Y-m-d_H-i-s") . ".sql";

// Comando para ejecutar la copia de seguridad
$command = "{$mysqldumpPath} --user={$username} --password={$password} {$database} > {$backupFolder}\\{$backupFileName}";

// Ejecutar el comando
exec($command, $output, $returnCode);

// Verificar si la copia de seguridad se realizó correctamente
if ($returnCode === 0) {
    echo "Copia de seguridad creada con éxito: {$backupFileName}";
} else {
    echo "Error al crear la copia de seguridad";
}
?>


//editar el archivo 
crontab -e
// agregarle la nueva tarea
0 12 * * * /usr/bin/php /ruta/a/backup_script.php
