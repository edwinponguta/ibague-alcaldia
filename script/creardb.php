<?php
$servername = "localhost";
$username = "root";
$password = "anlug1234";    
$dbname = "php_mysql";  


$conn = new mysqli($servername, $username, $password);


if ($conn->connect_error) {
    die("Conexión fallida: " . $conn->connect_error);
}

// Script SQL
$sql = "
CREATE DATABASE IF NOT EXISTS `php_mysql`;
USE `php_mysql`;
DROP TABLE IF EXISTS `usuarios`;


CREATE TABLE `usuarios` (
  `idUsuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idUsuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


LOCK TABLES `usuarios` WRITE;

INSERT INTO `usuarios` VALUES 
(1,'Edwin Ponguta','ingponguta@outlook.com','12345'),
(2,'Yesid Zambrano','edwin.ponguta.z@gmail.com','prueba1123'),
(3,'Carlos Trujillo','trujillo@gmail.com','trujillo123');

UNLOCK TABLES;";

// Ejecutar múltiples consultas
if ($conn->multi_query($sql) === TRUE) {
    echo "Script ejecutado correctamente";
} else {
    echo "Error al ejecutar el script: " . $conn->error;
}

// Cerrar la conexión
$conn->close();
?>
